* PSID data loading and generating basic family demographic variables
* Provided by Duke University Population Research Institute

* Clear all existing data
clear

* Load data
import delimited "psid_2021_demographics.csv", clear

# Renaming the variables to be more useful
rename ER78001 release_number
rename ER78002 fam_id_2021
rename ER78016 num_fu
rename ER78017 age_ref
rename ER78018 sex_ref
rename ER78019 age_spouse
rename ER78020 sex_spouse
rename ER78021 children_fu
rename ER78025 marital_status
rename ER78167 employment_1
rename ER78168 employment_2
rename ER78169 employment_3
rename ER78481 employment_spouse_1
rename ER78482 employment_spouse_2
rename ER78483 employment_spouse_3
rename ER79524 spouse_in_fu
rename ER81016 ethnicity_spouse
rename ER81017 race_spouse_1
rename ER81018 race_spouse_2
rename ER81019 race_spouse_3
rename ER81020 race_spouse_4
rename ER81143 ethnicity
rename ER81144 race_1
rename ER81145 race_2
rename ER81146 race_3
rename ER81147 race_4
rename ER81775 fam_income
rename ER81836 wealth_no_equity
rename ER81837 wealth_no_equity_accuracy
rename ER81838 wealth_equity
rename ER81839 wealth_equity_accuracy
rename ER81948 household_id


* ETHNICITY
* 9 (DK) recoded to NA by being left out below
* 0 (not Latino) recoded to 0
* all other responses recoded to 1
gen latino = .
replace latino = 1 if ethnicity > 0
replace latino = 0 if ethnicity == 0

* SPOUSE ETHNICITY
* 9 (DK) recoded to NA by being left out below
* 0 (not Latino) AND no Spouse/Partner in FU (spouse_in_fu=5) recoded to NA
* 0 (not Latino) ONLY recoded to 0
* all other responses recoded to 1
gen latino_spouse = .
replace latino_spouse = 1 if ethnicity_spouse > 0
replace latino_spouse = . if ethnicity_spouse == 9
replace latino_spouse = . if spouse_in_fu == 5
replace latino_spouse = 0 if ethnicity_spouse == 0 & spouse_in_fu == 1


* RACE
* 1 = White
* 2 = Black
* 3 = American Indian or Alaskan Native
* 4 = Asian
* 5 = Native Hawaiian or Pacific Islander
* 6 = 2 or more (recode from 4 separate variables)
* 7 = Other
* 9 = DK/NA (recoded to NA)
psid_2021 <- psid_2021 %>% mutate(race = case_when(
  race_1 == 1 & race_2 == 0 & race_3 == 0 & race_4 == 0 ~ 1,
  race_1 == 2 & race_2 == 0 & race_3 == 0 & race_4 == 0 ~ 2,
  race_1 == 3 & race_2 == 0 & race_3 == 0 & race_4 == 0 ~ 3,
  race_1 == 4 & race_2 == 0 & race_3 == 0 & race_4 == 0 ~ 4,
  race_1 == 5 & race_2 == 0 & race_3 == 0 & race_4 == 0 ~ 5,
  race_1 == 7 & race_2 == 0 & race_3 == 0 & race_4 == 0 ~ 7,
  race_1 < 9 & (race_2 > 0 | race_3 > 0 | race_4 > 0) ~ 6),
  .before = race_1)

* RACE SPOUSE
* 1 = White
* 2 = Black
* 3 = American Indian or Alaskan Native
* 4 = Asian
* 5 = Native Hawaiian or Pacific Islander
* 6 = 2 or more (recode from 4 separate variables)
* 7 = Other
* 9 = DK/NA (recoded to NA)
* 0 (not Latino) AND no Spouse/Partner in FU (spouse_in_fu=5) recoded to NA
* 0 (not Latino) ONLY recoded to 0
gen race_spouse = .
replace race_spouse = 1 if race_spouse_1 == 1 & race_spouse_2 == 0 & race_spouse_3 == 0 & race_spouse_4 == 0
replace race_spouse = 2 if race_spouse_1 == 2 & race_spouse_2 == 0 & race_spouse_3 == 0 & race_spouse_4 == 0
replace race_spouse = 3 if race_spouse_1 == 3 & race_spouse_2 == 0 & race_spouse_3 == 0 & race_spouse_4 == 0
replace race_spouse = 4 if race_spouse_1 == 4 & race_spouse_2 == 0 & race_spouse_3 == 0 & race_spouse_4 == 0
replace race_spouse = 5 if race_spouse_1 == 5 & race_spouse_2 == 0 & race_spouse_3 == 0 & race_spouse_4 == 0
replace race_spouse = 7 if race_spouse_1 == 7 & race_spouse_2 == 0 & race_spouse_3 == 0 & race_spouse_4 == 0
replace race_spouse = 6 if (race_spouse_1 < 9 & race_spouse_1 > 0) & (race_spouse_2 > 0 | race_spouse_3 > 0 | race_spouse_4 > 0)
replace race_spouse = . if race_spouse_1 == 0 & spouse_in_fu == 5
replace race_spouse = 0 if race_spouse_1 == 0 & spouse_in_fu == 1

* Recoding missing values
foreach var in age_ref age_spouse sex_spouse marital_status employment_1 employment_2 employment_3 ///
                     employment_spouse_1 employment_spouse_2 employment_spouse_3 ethnicity_spouse ///
                     race_spouse_1 race_spouse_2 race_spouse_3 race_spouse_4 ethnicity race_1 ///
                     race_2 race_3 race_4 fam_income wealth_no_equity wealth_no_equity_accuracy ///
                     wealth_equity wealth_equity_accuracy {
    replace `var' = . if `var' == 999 | `var' == 0 | `var' == 8 | `var' == 9 | ///
                        `var' == 99
}


